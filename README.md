# Alexa-mam-client

Sending command through IOTA Tangle with MAM client. 

In order for this to work well, you are invited to send your desired "action" and "target". It will be included in Alexa schema

This project is build on this [boilerplate] (https://github.com/martinabrahams/alexa-ask-sdk-typescript-boilerplate) for [ASK v2.0] (https://github.com/alexa/alexa-skills-kit-sdk-for-nodejs/tree/2.0.x/ask-sdk).
[IOTA MAM client] (https://github.com/iotaledger/mam.client.js) is used to send command to the IOTA tangle. 
 
## Dependencies

- nodejs 8.x
- typescript 

## Get started

- Run `npm install` to install dependencies
- Run `npm run build` to transpile JavaScript (./src) to TypeScript (./dist)

## Build Dist Version

Run `sh ./build.sh` to create the dist output, suitable for zipping and deploying to Lambda.

To run the demo on Alexa run the following command<br>
<code>alexa ask iota to reboot higgins</code>

This will send the following JSON message to the tangle
{
    time: <time>,
    action: <alexa skill's defined action>,
    target: <alexa skill's defined target>
}

This demo use a restricted chanel on a private node, feel free to change it for publically available IOTA node

## Export to AWS Lambda

After the build.sh script execution, you have a lambda.zip to use to update your lambda function.
To create the Alexa skill kit you have alexa-higgins-schema.json to start. 
You can see in the type definition the supported action / target to demo. 

## Warning 
This was working on previous IOTA release. If enough interest, an update will be pushed. 
