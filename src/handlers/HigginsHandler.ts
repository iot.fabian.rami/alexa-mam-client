import { HandlerInput, RequestHandler } from "ask-sdk"
import { Response } from "ask-sdk-model"  
const Mam = require('mam.client.js')

const {asciiToTrytes} = require('@iota/converter')
const util = require('util'); 

let node = 'http://ec2-52-59-164-192.eu-central-1.compute.amazonaws.com:14265' 
let mode = 'restricted'
let secretKey = 'thisiscustomersecret'
let mamState = Mam.init(node)
mamState = Mam.changeMode(mamState, mode, secretKey)
 
let publish = async (packet:any) => {
       
    const trytes = asciiToTrytes(JSON.stringify(packet))
    const message = Mam.create(mamState, trytes)

    mamState = message.state
    await Mam.attach(message.payload, message.address);
    console.log(message)
    
} 

export class HigginsHandler implements RequestHandler {
 
    canHandle(handlerInput: HandlerInput): boolean {
        const request = handlerInput.requestEnvelope.request; 
        console.log(`request : ${JSON.stringify(request, null, 2)}`);  
       return request.type === 'IntentRequest' && request.intent.name === 'higgins' ;
  }

    handle(handlerInput: HandlerInput): Response { //  Response | Promise<Response>
        const responseBuilder = handlerInput.responseBuilder;
        const request = handlerInput.requestEnvelope.request;
        console.log(`Higgins Handler init ${ new Date().toLocaleTimeString()}`);
        let cmd = {
            'time':`${ new Date().toLocaleTimeString()}`,
            'action':`${request.intent.slots.action.value}` ,
            'target':`${request.intent.slots.target.value}`
        };

        publish(cmd) 
        console.log(cmd)
               
        return  responseBuilder
        .speak('Sending command')
        .reprompt('Message is transmitted')
        .getResponse();
    }   
}